import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { PlaylistDetails } from "../playlists/components/PlaylistDetails";
import { Playlist } from "../core/model/Playlist";

export default {
  title: "Playlists/Details",
  component: PlaylistDetails,
  // argTypes: {
  //   backgroundColor: { control: "color" },
  // },
  decorators: [
    (Story) => (
      <div style={{ maxWidth: "200px", margin: "0 auto" }}>{Story()}</div>
    ),
  ],
} as ComponentMeta<typeof PlaylistDetails>;

const Template: ComponentStory<typeof PlaylistDetails> = (args) => (
  <PlaylistDetails {...args} />
);

const playlistMock: Playlist = {
  id: "123",
  type: "playlist",
  name: "Playlist 123",
  description: "",
  public: false,
  tracks: [],
};

export const Primary = Template.bind({});
Primary.args = {
  playlist: playlistMock,
};

export const Public = Template.bind({});
Public.args = {
  playlist: {
    ...playlistMock,
    public: true,
  },
};
