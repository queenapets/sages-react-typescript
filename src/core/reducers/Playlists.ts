import { Reducer, Dispatch } from "redux";
import { Playlist } from "../model/Playlist";
import { AppDispatch, AppState } from "../store";
import { fetchPlaylistById, fetchPlaylists } from "../services/PlaylistsAPI";

export const featureKey = 'playlists'

export const playlistsFeature = (state: AppState): PlaylistsState => state[featureKey]

interface PlaylistsState {
    items: Playlist[];
    selectedId?: Playlist["id"];
}

export const initialState: PlaylistsState = {
    items: [],
};


export const reducer: Reducer<PlaylistsState, Actions> = (
    state = initialState,
    action
) => {
    // console.log(action);

    switch (action.type) {
        case "PLAYLISTS_LOAD":
            return { ...state, items: action.payload.data };
        case "PLAYLISTS_DELETE":
            return {
                ...state,
                items: state.items.filter((p) => p.id !== action.payload.id),
            };
        case "PLAYLISTS_SELECT":
            return { ...state, selectedId: action.payload.id };
        case "PLAYLISTS_UPDATE":
            return {
                ...state, items: state.items.map(
                    (p) => p.id === action.payload.data.id ?
                        action.payload.data : p),
            };
        default:
            return state;
    }
};

/* ACtion Creators */

type Actions = ReturnType<
    | typeof playlistsLoad
    | typeof playlistsSelect
    | typeof playlistsDelete
    | typeof playlistsUpdate
>;

export const playlistsLoad = (data: Playlist[]) =>
({
    type: "PLAYLISTS_LOAD",
    payload: { data },
} as const);

export const playlistsUpdate = (data: Playlist) =>
({
    type: "PLAYLISTS_UPDATE",
    payload: { data },
} as const);

export const playlistsSelect = (id: Playlist["id"]) =>
({
    type: "PLAYLISTS_SELECT",
    payload: { id },
} as const);

export const playlistsDelete = (id: Playlist["id"]) =>
({
    type: "PLAYLISTS_DELETE",
    payload: { id },
} as const);


export const playlistsLoadAsync = async (dispatch: Dispatch<any>) => {
    const data = await fetchPlaylists();
    dispatch(playlistsLoad(data));
}

export const playlistsLoadByIdAsync = (id: Playlist['id']) => async (dispatch: Dispatch<any>) => {
    const data = await fetchPlaylistById(id);
    dispatch(playlistsSelect(data!.id));
}

/* State Selectors */

export const playlistSelectCurrent = (state: AppState) => {
    const feature = playlistsFeature(state)
    return feature.selectedId ? feature.items.find(p => p.id === feature.selectedId) : undefined
}