import React, { useContext } from "react";
import { UserContext } from "../contexts/UserContext";

export const UserProfile = () => {
  const context = useContext(UserContext);
//   const {messages} = useContext(InboxContext);

  if (!context) throw "No UserContext Provider!";

  if (!context.user)
    return (
      <div>
        Welcome Guest, <span onClick={context.login}>Login</span>
      </div>
    );

  return (
    <div>
      Welcome {context.user?.display_name},{" "}
      <span onClick={context.logout}>Logout</span>
    </div>
  );
};

/* 
  <UserContext.Consumer>
  {(context) => {
    if (!context) throw "No context!";

    return <div>Welcome {context.user?.display_name}, Logout</div>;
  }}
</UserContext.Consumer>; 
*/

/* 
  <UserContext.Consumer>
    {(user) => <InboxContext.Consumer>
        {(messages) => {
            return <div>Welcome {context.user?.display_name}, you have {messages.count} messages</div>;
        }
    </InboxContext.Consumer>}
</UserContext.Consumer>; 
*/
