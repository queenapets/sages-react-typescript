import axios, { CancelToken } from "axios";
import { Album, validateAlbumsSearchResponse } from "../model/Search";

export const searchAlbums = async (
  query: string,
  cancelToken?: CancelToken
): Promise<Album[]> => {
  const { data } = await axios.get(`https://api.spotify.com/v1/search`, {
    params: {
      type: "album",
      q: query,
    },
    cancelToken,
  });
  validateAlbumsSearchResponse(data);

  return data.albums.items;
};


export const getAlbumById = async (
  id: string,
  cancelToken?: CancelToken
): Promise<Album> => {
  const { data } = await axios.get<Album>(
    `https://api.spotify.com/v1/albums/${id}`,
    {
      cancelToken,
    }
  );

  return data;
};
