import axios from "axios";
import React, { useEffect, useState } from "react";
import { useAuth } from "../hooks/useAuth";
import { UserProfile } from "../model/UserProfile";

interface UserContextType {
  user: UserProfile | null;
  token: string | undefined;
  error?: string;
  login(): void;
  logout(): void;
}

export const UserContext = React.createContext<UserContextType | null>(null);

export const UserContextProvider: React.FC<{}> = ({ children }) => {
  const [token, getToken, setToken] = useAuth();
  const [user, setUser] = useState<UserProfile | null>(null);

  useEffect(() => {
    if (!token) {
      setUser(null);
      return;
    }

    axios
      .get<UserProfile>(`https://api.spotify.com/v1/me`)
      .then((resp) => setUser(resp.data))
      .catch((error) => {
        console.error(error);
      });
  }, [token]);

  const ctx: UserContextType = {
    user,
    token,
    login() {
      getToken();
      //   setUser({ display_name: "test" } as any);
    },
    logout() {
      setToken(undefined);
      //   setUser(null);
    },
  };

  return <UserContext.Provider value={ctx}>{children}</UserContext.Provider>;
};
