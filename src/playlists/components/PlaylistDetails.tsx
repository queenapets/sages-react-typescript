import React from "react";
import { Playlist } from "../../core/model/Playlist";
import styles from "./PlaylistDetails.module.css";

interface Props {
  playlist?: Playlist;
  /**
   * Emit Edit button click
   * @param id Playlist id
   */
  onEdit(id: Playlist["id"]): void;
}

export const PlaylistDetails = React.memo(({ playlist, onEdit }: Props) => {
  if (!playlist) {
    return <p className="alert alert-info mb-3">Please select playlist</p>;
  }

  return (
    <div>
      <dl title={playlist.name} data-playlist-id={playlist.id}>
        <dt>Name:</dt>
        <dd data-testid="playlist-name">{playlist.name}</dd>
        <dt>Public:</dt>
        {/* <dd style={{ color: playlist.public ? 'red' : 'green' }}> */}
        <dd
          aria-label="Playlist public"
          className={
            styles.playlistVisibility +
            " " +
            (playlist.public ? "text-danger" : "text-success")
          }
        >
          {playlist.public ? "Yes" : "No"}
        </dd>
        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={() => onEdit(playlist.id)}>
        Edit
      </button>
    </div>
  );
});
