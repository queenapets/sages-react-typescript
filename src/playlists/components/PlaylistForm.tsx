import React, { useEffect, useRef, useState } from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist: Playlist;
  onCancel(): void;
  onSave(szkic: Playlist): void;
}

export const PlaylistForm = React.memo(
  ({ playlist: playlistFromParent, onCancel, onSave }: Props) => {
    const [playlistName, setPlaylistName] = useState(playlistFromParent.name);
    const [playlistPublic, setPlaylistPublic] = useState(
      playlistFromParent.public
    );
    const [playlistDescription, setDescription] = useState(
      playlistFromParent.description
    );

    const inputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
      setPlaylistName(playlistFromParent.name);
      setPlaylistPublic(playlistFromParent.public);
      setDescription(playlistFromParent.description);
    }, [playlistFromParent]);

    useEffect(() => {
      console.log("after first render");
      // const inputRef = document.getElementById('PlaylistForm__name')
      // inputRef?.focus()
      inputRef.current?.focus();
    }, []);

    const save = () => {
      onSave({
        // id: playlistFromParent.id,
        ...playlistFromParent,
        name: playlistName,
        description: playlistDescription,
        public: playlistPublic,
      });
    };

    return (
      <div>
        <pre>{JSON.stringify(playlistFromParent, null, 2)}</pre>
        <pre>
          {JSON.stringify(
            {
              name: playlistName,
              description: playlistDescription,
              public: playlistPublic,
            },
            null,
            2
          )}
        </pre>
        <div className="form-group mb-3">
          <label htmlFor="PlaylistForm__name">Name:</label>
          <input
            type="text"
            className="form-control"
            name="name"
            id="PlaylistForm__name"
            // ref={(elem) => console.log(elem)}
            ref={inputRef}
            aria-describedby="helpId"
            onChange={(e) => setPlaylistName(e.currentTarget.value)}
            value={playlistName}
          />
          <small id="helpId" className="form-text text-muted">
            {playlistName.length} / 120
          </small>
        </div>

        <div className="form-check mb-3">
          <label className="form-check-label">
            <input
              type="checkbox"
              className="form-check-input"
              name="public"
              onChange={(event) =>
                setPlaylistPublic(event.currentTarget.checked)
              }
              checked={playlistPublic}
            />
            Public
          </label>
        </div>

        <div className="form-group mb-3">
          <label htmlFor="">Description:</label>
          <textarea
            className="form-control"
            name="description"
            rows={3}
            value={playlistDescription}
            onChange={(e) => setDescription(e.currentTarget.value)}
          />
        </div>

        <button className="btn btn-danger" onClick={onCancel}>
          Cancel
        </button>
        <button className="btn btn-success" onClick={save}>
          Save
        </button>
      </div>
    );
  }
);

// PlaylistForm.defaultProps = {
//   playlist: { id: "", name: "", description: "", public: false },
// };

// const PlaylistName = () => {
//     const [playlistName, setPlaylistName] = useState(playlist.name)

//     const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
//         // console.log(event.currentTarget.value)
//         setPlaylistName(event.currentTarget.value)
//         console.log(event.currentTarget.value, playlistName)
//     }

//     return <div className="form-group mb-3">
//         <label htmlFor="PlaylistForm__name">Name:</label>
//         <input type="text"
//             className="form-control"
//             name="name"
//             id="PlaylistForm__name"
//             aria-describedby="helpId"
//             onChange={handleNameChange}
//             value={playlistName} />
//         <small id="helpId" className="form-text text-muted">{playlistName.length} / 120</small>
//     </div>

// }

// interface Props {}
// interface State {}

// export class PlaylistForm2 extends React.Component<Props, State> {
//   state = {};

//   constructor(props: Props) {
//     super(props);
//   }
//   componentDidMount() {}
//   componentDidUpdate() {}
//   componentWillUnmount() {}

//   static getDerivedStateFromProps(props: Props) {
//     return {
//       playlist: props.playlist,
//     };
//   }
//   // shouldComponentUpdate(){}

//   updatePLaylistName(newName: string) {
//     this.setState({
//       playlist: {
//         ...this.state,
//         name: newName,
//       },
//     });
//   }

//   render() {
//     return <div></div>;
//   }
// }
