import React, { useEffect } from "react";
import { Album, SimpleAlbum } from "../../core/model/Search";
import { getAlbumById, searchAlbums } from "../../core/services/SearchAPI";
import { ResultsGrid } from "../components/ResultsGrid";
import { SearchForm } from "../components/SearchForm";
import { useSearch } from "../../core/hooks/useSearch";
import { RouteComponentProps } from "react-router-dom";

interface Props extends RouteComponentProps {}

export const AlbumsSearch = (props: Props) => {
  const initialQuery: string = "batman";

  const [{ loading, message, query, results = [] }, setSearchQuery] = useSearch(
    searchAlbums,
    initialQuery
  );

  useEffect(() => {
    const q = new URLSearchParams(props.location.search).get("q");
    q && setSearchQuery(q);
  }, [props.location.search]);

  const search = (query: string) => {
    props.history.push("/search?q=" + query);
    // setSearchQuery(query) // should be done on navigation ^
  };

  return (
    <div>
      {/* .row*2>.col */}
      <div className="row mb-3">
        <div className="col">
          <SearchForm onSearch={search} query={query} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {loading && <p className="alert alert-info">Loading</p>}
          {message && <p className="alert alert-danger">{message}</p>}

          <ResultsGrid results={results} />
        </div>
      </div>
    </div>
  );
};
