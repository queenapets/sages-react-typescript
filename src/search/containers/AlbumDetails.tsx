import React, { useEffect, useState } from "react";
import { AlbumCard } from "../components/AlbumCard";

import { useLocation, RouteComponentProps, useParams } from "react-router-dom";
import { getAlbumById } from "../../core/services/SearchAPI";
import { Album } from "../../core/model/Search";
import { TracksList } from "./TracksList";

type RouterParams = {
  album_id: string;
};

interface Props extends RouteComponentProps<RouterParams> {}

export const AlbumDetails = (props: Props) => {
  // props.match.params.album_id

  const [album, setAlbum] = useState<Album | null>(null);
  const [message, setMessage] = useState("");

  const { album_id } = useParams<RouterParams>();
  // const { search } = useLocation();
  // const id = new URLSearchParams(search).get("id");

  const updateAlbum = async (id: Album["id"]) => {
    try {
      const result = await getAlbumById(id);
      setAlbum(result);
    } catch (error) {
      setMessage(error.message);
    }
  };

  useEffect(() => {
    album_id && updateAlbum(album_id);
  }, [album_id]);

  if (message) {
    return <p className="alert alert-danger">{message}</p>;
  }

  if (!album) {
    return <p className="alert alert-info">Loading</p>;
  }

  return (
    <div>
      <div className="row">
        <div className="col">
          <small className="text-muted">{album_id}</small>
          <h3> {album.name} </h3>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <AlbumCard album={album}>
            <dl>
              <hr />
              <dt>Artist Name</dt>
              <dd>{album.artists[0].name}</dd>
              <dt>Release Date</dt>
              <dd>{album.release_date}</dd>
            </dl>
          </AlbumCard>
        </div>
        <div className="col">
          <TracksList tracks={album.tracks.items} />
        </div>
      </div>
    </div>
  );
};
