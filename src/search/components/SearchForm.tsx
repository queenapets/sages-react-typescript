import React, { useEffect, useRef, useState } from "react";

interface Props {
  query: string;
  onSearch: (query: string) => void;
  // onSearch(query: string): void
}

export const SearchForm = ({ onSearch, query: parentQuery }: Props) => {
  const [query, setQuery] = useState(parentQuery);
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputRef.current?.focus();
  }, []);

  useEffect(() => {
    setQuery(parentQuery);
  }, [parentQuery]);

  useEffect(() => {
    if (query === parentQuery) {
      return;
    }
    const handle = setTimeout(() => onSearch(query), 500);

    return () => clearTimeout(handle);
  }, [query]);

  return (
    <div>
      <div className="input-group">
        <input
          ref={inputRef}
          type="text"
          className="form-control"
          name="search"
          id="SearchForm__search"
          placeholder="Search albums"
          aria-label="Search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          onKeyDown={(e) => e.code === "Enter" && onSearch(query)}
        />
        <button
          className="btn btn-secondary"
          type="button"
          onClick={() => onSearch(query)}
        >
          Search
        </button>
      </div>
    </div>
  );
};
