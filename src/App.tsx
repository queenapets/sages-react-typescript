/// <reference path="../index.d.ts"/>

import { useState } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { PlaylistsView } from "./playlists/containers/PlaylistsView";
import { AlbumsSearch } from "./search/containers/AlbumsSearch";

import { OAuthCallback } from "react-oauth2-hook";
import { Route, Switch, Redirect, NavLink } from "react-router-dom";
import { AlbumDetails } from "./search/containers/AlbumDetails";
import { UserProfile } from "./core/containers/UserProfile";
import { PlaylistsReduxView } from "./playlists/containers/PlaylistsReduxView";

function App() {
  const [menuOpen, setMenuOpen] = useState(false);


  return (
    <div className="App">
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div className="container">
          <NavLink className="navbar-brand" to="/">
            MusicApp
          </NavLink>

          <button
            className="navbar-toggler"
            type="button"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
            onClick={() => setMenuOpen(!menuOpen)}
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div
            className={"collapse navbar-collapse " + (menuOpen ? "show" : "")}
            id="navbarText"
          >
            <ul className="navbar-nav me-auto mb-2 mb-sm-0">
              <li className="nav-item">
                <NavLink
                  className="nav-link"
                  to="/search"
                  activeClassName="placki active"
                >
                  Search
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">
                  Playlists
                </NavLink>
              </li>
            </ul>
            <span className="navbar-text">
              <button className="btn btn-default  float-end text-light">
                {/* <UserContextProvider> */}
                  <UserProfile />
                {/* </UserContextProvider> */}
              </button>
            </span>
          </div>
        </div>
      </nav>

      <div className="container">
        <div className="row">
          <div className="col">
            <Switch>
              <Redirect path="/" exact to="/playlists" />
              <Route path="/playlists" component={PlaylistsReduxView} />
              <Route path="/search" exact component={AlbumsSearch} />
              <Route path="/search/albums/:album_id" component={AlbumDetails} />
              <Route path="/callback" component={OAuthCallback} />
              <Route
                path="**"
                render={() => (
                  <div className="display-flex text-center ">
                    <h1 className="m-auto">Page not Found</h1>
                  </div>
                )}
              />
              {/* <Route path="**" component={PageNotFound} />
              <Redirect path="**"  to='/playlists' /> */}
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
