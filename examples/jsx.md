```tsx

window.React = React
window.ReactDOM = ReactDOM

interface User {
  name: string;
  /**
   * User favourite animal
   */
  pet: string;
}

const users: User[] = [
  { name: 'Alice', pet: 'kota' },
  { name: 'Catlyn', pet: 'rybki' },
  { name: 'Bob', pet: 'psa' },
]

// const MojComponent = ({ user }: { user: User }) => <div title={user.name}> {user.name} </div>;

// <MojComponent user={users[0]} />;

const UserCard = (props: { user: User }) => <div>
  {props.user.name} ma {props.user.pet} i <input /> !
</div>

ReactDOM.render(<div>
  <ul>
    {users.map(listUser => <li key={listUser.name}>
      <UserCard user={listUser} />
    </li>)}
  </ul>
</div>, document.getElementById('root'))
```